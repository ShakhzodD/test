import {
  useQuery,
  QueryFunctionContext,
  UseQueryResult,
} from "@tanstack/react-query";
import api from "api";

interface QueryKeyArgs {
  url: string;
}

interface Props {
  name: string;
  url: string;
}

async function getAll({
  queryKey,
}: QueryFunctionContext<[string, QueryKeyArgs]>) {
  const { url } = queryKey[1];

  const res = await api.get(url);
  return res.data;
}
function useGetAll(args: Props): UseQueryResult {
  const { name, url } = args;
  const data = useQuery({
    queryKey: [`${name}`, { url }],
    queryFn: getAll,
  });

  return { ...data };
}
export default useGetAll;
