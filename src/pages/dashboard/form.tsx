import React from "react";
import { Fields } from "components";
import { Form, FastField } from "formik";
import { Button } from "antd";
const FormComponent = ({ isLoading }: { isLoading: boolean }) => {
  return (
    <div>
      <Form>
        <FastField
          name="full_name"
          type="text"
          component={Fields.Input}
          label="Full name"
        />
        <FastField
          name="amount"
          type="number"
          component={Fields.Input}
          label="Amount"
        />
        <FastField
          name="transaction"
          type="text"
          component={Fields.Input}
          label="Transaction"
        />
        <FastField
          name="currency_type"
          component={Fields.Select}
          label="Currency type"
          options={[
            { label: "Click", value: "click" },
            { label: "Payme", value: "payme" },
            { label: "Naqt", value: "naqt" },
            { label: "Plastik", value: "plastik" },
          ]}
        />

        <Button
          className="mt-5"
          loading={isLoading}
          type="primary"
          htmlType="submit"
        >
          Submit
        </Button>
      </Form>
    </div>
  );
};

export default FormComponent;
