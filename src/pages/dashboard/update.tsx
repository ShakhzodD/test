import { useQueryClient } from "@tanstack/react-query";
import { Formik } from "formik";
import usePost from "hooks/useMutate";
import { FC } from "react";
import * as Yup from "yup";
import Form from "./form";
import { message } from "antd";
import { get } from "lodash";
interface IUpdateProps {
  setUpdateModal: Function;
  updateModal: {
    modal: boolean;
    data: object | null;
  };
}
const Update: FC<IUpdateProps> = ({ setUpdateModal, updateModal }) => {
  console.log(updateModal);

  const { mutate, isLoading } = usePost();
  const queryClient = useQueryClient();
  const schema = Yup.object().shape({
    full_name: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
    amount: Yup.number(),
    currency_type: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
    transaction: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
  });
  return (
    <div>
      <Formik
        initialValues={{
          full_name: get(updateModal, "data.full_name") || "",
          amount: get(updateModal, "data.amount"),
          currency_type: get(updateModal, "data.currency_type"),
          transaction: get(updateModal, "data.transaction"),
        }}
        validationSchema={schema}
        onSubmit={value => {
          mutate(
            {
              url: `/list/${get(updateModal, "data.id")}`,
              data: value,
              method: `post`,
            },
            {
              onSuccess: () => {
                queryClient.invalidateQueries({ queryKey: ["list"] });
                message.success({
                  content: "Updated successfully",
                  duration: 2,
                });
                setUpdateModal({ modal: false, data: null });
              },
            }
          );
        }}
      >
        {({ values, setFieldValue }) => <Form {...{ isLoading }} />}
      </Formik>
    </div>
  );
};

export default Update;
