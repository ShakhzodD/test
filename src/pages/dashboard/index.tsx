import { UseQueryResult, useQueryClient } from "@tanstack/react-query";
import { Button, Modal, Spin, Table, message } from "antd";
import useGetAll from "hooks/useGetAll";
import { useState } from "react";
import Create from "./create";
import Update from "./update";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { get } from "lodash";
import usePost from "hooks/useMutate";
const Dashboard = () => {
  const queryClient = useQueryClient();
  const [openModal, setOpenModal] = useState<boolean>(false);

  const [updateModal, setUpdateModal] = useState<{
    modal: boolean;
    data: null;
  }>({ modal: false, data: null });
  const { data, isLoading }: { data: undefined | any } & UseQueryResult =
    useGetAll({
      url: "/list",
      name: "list",
    });
  const { mutate } = usePost();
  const deleteHandler = (id: number) => {
    mutate(
      { url: `/list/${id}`, data: null, method: "delete" },
      {
        onSuccess: () => {
          queryClient.invalidateQueries({ queryKey: ["list"] });
          message.success({
            content: "Deleted successfully",
            duration: 2,
          });
        },
      }
    );
  };

  return (
    <div>
      <Modal
        open={openModal}
        onOk={() => setOpenModal(true)}
        onCancel={() => setOpenModal(false)}
        footer={null}
        centered
        title="Create"
        width={550}
        destroyOnClose
      >
        {openModal && <Create {...{ setOpenModal }} />}
      </Modal>
      <Modal
        open={updateModal.modal}
        onOk={() => setUpdateModal({ modal: false, data: null })}
        onCancel={() => setUpdateModal({ modal: false, data: null })}
        footer={null}
        centered
        title="Update"
        width={550}
        destroyOnClose
      >
        {updateModal.modal && <Update {...{ setUpdateModal, updateModal }} />}
      </Modal>
      <div className="flex justify-end mb-5">
        <Button
          size="large"
          type="primary"
          onClick={() => {
            setOpenModal(true);
          }}
        >
          Create
        </Button>
      </div>
      <Spin spinning={isLoading}>
        <Table
          rowKey={"id"}
          columns={[
            {
              title: "Full Name",
              dataIndex: "full_name",
              render: value => {
                return <>{value}</>;
              },
            },

            {
              title: "Amount",
              dataIndex: "amount",
              render: value => {
                return <>{value}</>;
              },
            },
            {
              title: "Currency",
              dataIndex: "currency_type",
              render: value => {
                return <>{value}</>;
              },
            },
            {
              title: "Transaction",
              dataIndex: "transaction",
              render: value => {
                return <>{value}</>;
              },
            },
            {
              title: "",
              dataIndex: "transaction",
              render: (value, row) => {
                return (
                  <EditOutlined
                    onClick={() => {
                      setUpdateModal({ modal: true, data: row });
                    }}
                  />
                );
              },
            },
            {
              title: "",
              dataIndex: "transaction",
              render: (value, row) => {
                return (
                  <DeleteOutlined
                    onClick={() => {
                      deleteHandler(get(row, "id"));
                    }}
                  />
                );
              },
            },
          ]}
          dataSource={data}
        />
      </Spin>
    </div>
  );
};

export default Dashboard;
