import { useQueryClient } from "@tanstack/react-query";
import { message } from "antd";
import { Formik } from "formik";
import usePost from "hooks/useMutate";
import * as Yup from "yup";
import Form from "./form";
const schema = Yup.object().shape({
  full_name: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  amount: Yup.number(),
  currency_type: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  transaction: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
});
const Create = ({ setOpenModal }: { setOpenModal: Function }) => {
  const { mutate, isLoading } = usePost();
  const queryClient = useQueryClient();
  return (
    <div>
      <Formik
        initialValues={{
          full_name: "",
          amount: 0,
          currency_type: "",
          transaction: "",
        }}
        validationSchema={schema}
        onSubmit={value => {
          mutate(
            { url: "/list", data: value, method: "post" },
            {
              onSuccess: () => {
                queryClient.invalidateQueries({ queryKey: ["list"] });
                message.success({
                  content: "Added successfully",
                  duration: 2,
                });
                setOpenModal(false);
              },
            }
          );
        }}
      >
        {({ values, setFieldValue }) => <Form {...{ isLoading }} />}
      </Formik>
    </div>
  );
};

export default Create;
