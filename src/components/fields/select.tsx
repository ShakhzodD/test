import { Select } from "antd";
import { FieldProps } from "formik";
import { FC } from "react";
interface IAntSelect extends FieldProps<any, any> {
  options: any[];
  isClearable?: boolean;
  isSearchable?: boolean;
  label?: string;
  onChange?: Function;
  placeholder?: string;
  errorMessage?: string | any;
  rootClassName?: string;
}

const AntSelect: FC<IAntSelect> = (props: IAntSelect) => {
  const {
    options,
    isClearable,
    label,
    isSearchable,
    field: { name, value },
    placeholder = "Введите",
    errorMessage,
    onChange = () => {},
    form: { setFieldValue, setFieldTouched, errors, touched },
  } = props;

  return (
    <div>
      {label && <h1 className="mb-1 text-base ant-label">{label}</h1>}
      <Select
        className={`w-full font-medium `}
        key={name}
        size="large"
        status={touched[name] && errors[name] ? "error" : ""}
        value={value}
        allowClear={isClearable}
        showSearch={isSearchable}
        filterOption={(input, option) =>
          (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
        }
        onBlur={() => {
          if (!value) {
            setFieldTouched(name, true);
          }
        }}
        placeholder={placeholder}
        onChange={value => {
          onChange(value);
          setFieldValue(name, value);
        }}
        options={options}
      />
      {touched[name] && errors[name] && (
        <small className="text-xs font-semibold text-red-500">
          {errorMessage ? errorMessage : errors[name]}
        </small>
      )}
    </div>
  );
};

export default AntSelect;
