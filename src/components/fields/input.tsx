import { Input } from "antd";
import { FieldProps } from "formik";
import { HTMLInputTypeAttribute } from "react";

interface Props extends FieldProps<any, any> {
  label?: string;
  type?: HTMLInputTypeAttribute;
  className?: string;
  placeholder?: string;
  errorMessage?: string | undefined | any;
}

const input = (props: Props) => {
  const {
    field: { value, name },
    label,
    className,
    placeholder = "Введите",
    errorMessage,
    type = "text",
    form: { setFieldValue, setFieldTouched, errors, touched },
  } = props;

  return (
    <div>
      {label && <h1 className="mb-1 text-base ant-label">{label}</h1>}
      <div>
        <Input
          type={type}
          size="large"
          onBlur={() => !value && setFieldTouched(name, true)}
          value={value}
          placeholder={placeholder}
          className={`
            ${className}
            rounded-[4px]  
            placeholder:font-medium 
            placeholder:tracking-tighter 
          `}
          status={touched[name] && errors[name] ? "error" : ""}
          onChange={e => setFieldValue(name, e.target.value)}
        />
        {touched[name] && errors[name] && (
          <small className="text-xs font-semibold text-red-500">
            {errorMessage ? errorMessage : errors[name]}
          </small>
        )}
      </div>
    </div>
  );
};

export default input;
